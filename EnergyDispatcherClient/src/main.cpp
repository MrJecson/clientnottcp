#include "widget.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    EnergyDispatcherClient w;
    // Передать в init путь из cfg
    w.initClient();
    w.show();

    return a.exec();
}
