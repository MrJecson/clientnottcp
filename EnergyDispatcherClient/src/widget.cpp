#include "widget.h"

//-----------------------------------------------------------------------------
// КОНСТРУКТОР
//-----------------------------------------------------------------------------
EnergyDispatcherClient::EnergyDispatcherClient(QWidget *parent)
    : QWidget(parent)
    , edit_(new QTextEdit(this))
    , socketClient_(new QTcpSocket(this))
    , timerRequester_(new QTimer(this))
{
    edit_->resize(500, 300);
}

//-----------------------------------------------------------------------------
// ДЕСТРУКТОР
//-----------------------------------------------------------------------------
EnergyDispatcherClient::~EnergyDispatcherClient()
{

}


//-----------------------------------------------------------------------------
// Инициализация Tcp клиента
//-----------------------------------------------------------------------------
void EnergyDispatcherClient::initClient()
{
    // Экземпляр Cfg НЕ ЗАБЫТЬ!!!

//    if (cfg.load(ЗДЕСЬ ДОЛЖЕН БЫТЬ ПУТЬ!!!))
//    {
//        QString secName = "TcpConfig";
//        cfg.getString(secName, "Host", config.host_addr);

//        int port = 1995;

//        if (cfg.getInt(secName, "Port", port))
//        {

    // ВМЕСТО CONFIG -> CLIENT_CONF;
//            config.port = static_cast<quint16>(port);
//        }

//        cfg.getInt(secName, "ReconnectInterval", config.reconnect_interval);
//    }

    socketClient_->setProxy(QNetworkProxy(QNetworkProxy::NoProxy));

    connect(socketClient_, &QTcpSocket::connected, this, &EnergyDispatcherClient::onConnectToServer);
    connect(socketClient_, &QTcpSocket::disconnected, this, &EnergyDispatcherClient::onDisconnectToServer);
    connect(socketClient_, &QTcpSocket::readyRead, this, &EnergyDispatcherClient::slotReciveData);

    // Таймер для подключения клиента!
    connect(timerRequester_, &QTimer::timeout, this, &EnergyDispatcherClient::onTimerReconnect);
    timerRequester_->setInterval(client_conf_.reconnect_interval);
    timerRequester_->start();
}

//-----------------------------------------------------------------------------
// Подключение клиента!
//-----------------------------------------------------------------------------
void EnergyDispatcherClient::onConnectToServer()
{
    if (socketClient_->state() == QTcpSocket::ConnectedState)
    {
        timerRequester_->stop();

        //-------------------------------------------------------------
        // Блок отправки данных!
        //-------------------------------------------------------------
        data_client_.stateObjectSVG = true;

        QByteArray output_data;

        output_data.resize(sizeof (data_client_t));

        qDebug() << output_data;

        output_data = this->data_client_.serialize();

        qDebug() << output_data;

        // Проверка на открытый сокет клиента!
        if (socketClient_->isOpen())
        {
            // Передаем данные клиенту!
            socketClient_->write(output_data);

            // Запись из буфера в сетевой сокет! (Проверка на передачу данных, если ничего не передано, то false!)
            socketClient_->flush();
        }
        //-------------------------------------------------------------
    }
    else
    {
        return;    /// Не факт!
    }
}

//-----------------------------------------------------------------------------
// Остановка таймера и прочие действия при отключении TCP-клиента
//-----------------------------------------------------------------------------
void EnergyDispatcherClient::onDisconnectToServer()
{
    socketClient_->disconnectFromHost();

    if(socketClient_->state() == QTcpSocket::UnconnectedState)
    {
        qDebug() << "Отключение!";
    }
}

//-----------------------------------------------------------------------------
// Обработка таймаута таймера
//-----------------------------------------------------------------------------
void EnergyDispatcherClient::onTimerReconnect()
{
    // Вернуть если клиент подключен!
    if(socketClient_->state() == QTcpSocket::ConnectedState)
        return;

    socketClient_->abort();
    socketClient_->connectToHost(client_conf_.host_addr, client_conf_.port, QIODevice::ReadWrite);
}

// ПОМЕНЯТЬ НАЗВАНИЕ!
void EnergyDispatcherClient::slotReciveData()
{
    //-------------------------------------------------------------
    // Блок приема данных!
    //-------------------------------------------------------------
    QByteArray  input_data = socketClient_->readAll();

    qDebug() << input_data;

    // Проверка на соответствие размерностей!
//    if (static_cast<size_t>(input_data.size()) != sizeof (data_client_t))
//    {
//        qDebug() << "Invalid data size!";
//        return;
//    }

    this->data_client_.deserialize(input_data);

    qDebug() << input_data;

    qDebug() << data_client_.nameObjectSVG;
    qDebug() << data_client_.stateObjectSVG;
}

