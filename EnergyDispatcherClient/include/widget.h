#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>

#include <QTextEdit>
#include <QTcpSocket>
#include <QTimer>
#include <QDataStream>

#include <QNetworkProxy>

#include <QMessageBox>

#include <QDebug>

//Структура конфигураций клиента (Возможны изменения!)
struct client_config_t
{
    /// Адрес хоста!
    QString host_addr;

    /// Порт Сервера!
    quint16 port;

    /// Интервал переподключений!
    int  reconnect_interval;

    int  send_interval;

    client_config_t()
        : host_addr("127.0.0.1")
        , port(1995)
        , reconnect_interval(500)
        , send_interval(500)
    {

    }

};

#pragma pack(push, 1)
struct data_client_t
{
    /// Имя объекта передоваемое от клиента! (SVG файл!)
    QString     nameObjectSVG;

    /// Состояние объекта возвращаемое сервером!
    bool    stateObjectSVG;

    data_client_t()
        : nameObjectSVG("П12")
        , stateObjectSVG(false)
    {

    }

    // Метод для перевода данных в последовательность байт,
    // необходимый для отправки данных Серверу!
    QByteArray serialize()
    {
        QByteArray data_out;
        QDataStream data_s(&data_out, QIODevice::WriteOnly);

        data_s << nameObjectSVG
               << stateObjectSVG;

        return data_out;
    }

    // Метод для извлечения данных из последовательности байт,
    // необходимый для приема данных от Сервера!
    QByteArray deserialize(QByteArray &data_in)
    {
        QDataStream data_ds(&data_in, QIODevice::ReadOnly);

        data_ds >> nameObjectSVG
                >> stateObjectSVG;

        return data_in;
    }
};
#pragma pack(pop)

class EnergyDispatcherClient : public QWidget
{
    Q_OBJECT

public:

    EnergyDispatcherClient(QWidget *parent = Q_NULLPTR);

    ~EnergyDispatcherClient();

    void initClient();


private:

    QTextEdit   *edit_;

    QTcpSocket  *socketClient_;      ///< Сокет подключения!

    QTimer      *timerRequester_;    ///< Таймер запросов на подключение к серверу

    client_config_t client_conf_;   ///< Структура конфига клиента

    data_client_t   data_client_;   ///< Структура данных клиента


private slots:
    /// Запуск таймера и прочие действия при подключении TCP-клиента
    void onConnectToServer();

    /// Остановка таймера и прочие действия при отключении TCP-клиента
    void onDisconnectToServer();

    /// Обработка таймаута таймера
    void onTimerReconnect();

    /// Получение обмена данными
    void slotReciveData();
};
#endif // WIDGET_H

